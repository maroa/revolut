variable "region" {
  default = "us-east-2"
}
variable "cidr" {
  default = "10.170.0.0/16"
}

variable "ecr_name" {
  default = "hello-app"
}

variable "app_name" {
  default = "hello-app"
}

variable "image_tag" {
  default = "latest"
}

variable "cluster_name" {
  default = "eks"
}

variable "newbits" {
  default = 2
}

variable "db_name" {
  default = "hello_app"
}

variable "db_user" {
  default = "hello_app"
}