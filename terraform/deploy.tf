resource "null_resource" "docker-image" {
  depends_on = [aws_ecr_repository.ecr]
  triggers = {
    ids = "${aws_ecr_repository.ecr.arn}-${var.image_tag}-1"
  }
  provisioner "local-exec" {
    working_dir = "../app"
    command = <<EOT
      $(aws ecr get-login --no-include-email --region "${var.region}")
      docker build -t "${aws_ecr_repository.ecr.repository_url}:${var.image_tag}" .
      docker push "${aws_ecr_repository.ecr.repository_url}:${var.image_tag}"
   EOT
  }
}

resource "local_file" "alb_ingress" {
  filename = "../k8s/alb-ingress-controller.yaml"
  content = templatefile("template/alb-ingress-controller.yaml", { vpc_id = aws_vpc.main.id, cluster_name = var.cluster_name, region=var.region })
  depends_on = [aws_eks_cluster.eks, aws_eks_node_group.nodes]
}

resource "local_file" "app_deploy" {
  filename = "../k8s/app_deployment.yaml"
  content = templatefile("template/app_deployment.yaml",
  { repository_url = aws_ecr_repository.ecr.repository_url, image_tag=var.image_tag, db_host = aws_rds_cluster.postgresql.endpoint,
  db_user = var.db_user, db_name = var.db_name, db_password = random_password.password.result})

  depends_on = [aws_eks_cluster.eks, aws_eks_node_group.nodes, aws_rds_cluster.postgresql, aws_ecr_repository.ecr]
}

resource "null_resource" "app_deploy_k8s" {
  depends_on = [local_file.alb_ingress, local_file.app_deploy, aws_rds_cluster.postgresql,
    aws_rds_cluster_instance.cluster_instances, aws_route_table_association.private, aws_route_table_association.public,
    aws_route.public, aws_route.private-default-nat]
  triggers = {
    id = local_file.app_deploy.id
  }
  provisioner "local-exec" {
    working_dir = "../k8s"
    command = <<EOT
      aws eks --region ${var.region} update-kubeconfig --name ${var.cluster_name}
      kubectl apply -f rbac-role.yaml
      kubectl apply -f alb-ingress-controller.yaml
      kubectl apply -f app_namespace.yaml
      kubectl apply -f app_deployment.yaml
   EOT
  }
  provisioner "local-exec" {
    when = destroy
    working_dir = "../k8s"
    command = "[ -f app_deployment.yaml ] && kubectl delete -f app_deployment.yaml"
  }
  provisioner "local-exec" {
    when = destroy
    command = <<EOT
        while [ -n "$(aws ec2 describe-security-groups --filters Name=tag:'kubernetes.io/ingress-name',Values='hello-ingress' | jq .SecurityGroups[])" ]; do sleep 5; done
    EOT
  }
}