data "aws_availability_zones" "available" {}

data "http" "workstation_external_ip" {
  url = "http://ipinfo.io/ip"
}

locals {
  workstation_external_cidr = "${chomp(data.http.workstation_external_ip.body)}/32"
}

resource "random_password" "password" {
  length = 32
  special = false
}