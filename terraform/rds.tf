resource "aws_security_group" "rds" {
  name        = "postgres"
  vpc_id      = aws_vpc.main.id
  depends_on = [aws_vpc.main, aws_subnet.private]

  tags = {
    Name = "postgres"
  }

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = aws_subnet.private[*].cidr_block
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_db_subnet_group" "rds_subnet_group" {
  name        = "postgresql-subnet-group"
  subnet_ids  = aws_subnet.private[*].id
  depends_on = [aws_subnet.private]
}

resource "aws_rds_cluster_instance" "cluster_instances" {
  count                = 2
  identifier           = "cluster-postgresql${count.index}"
  cluster_identifier   = aws_rds_cluster.postgresql.id
  instance_class       = "db.t3.medium"
  engine               = "aurora-postgresql"
  db_subnet_group_name = aws_db_subnet_group.rds_subnet_group.name
  depends_on = [aws_db_subnet_group.rds_subnet_group, aws_rds_cluster.postgresql]
}

resource "aws_rds_cluster" "postgresql" {
  cluster_identifier      = "cluster-postgresql"
  engine                  = "aurora-postgresql"
  availability_zones      = data.aws_availability_zones.available.names
  db_subnet_group_name    = aws_db_subnet_group.rds_subnet_group.name
  database_name           = var.db_name
  master_username         = var.db_user
  master_password         = random_password.password.result
  backup_retention_period = 5
  preferred_backup_window = "05:00-07:00"
  vpc_security_group_ids  = [aws_security_group.rds.id]
  skip_final_snapshot     = true
  depends_on = [aws_db_subnet_group.rds_subnet_group, aws_security_group.rds]
}