resource "aws_vpc" "main" {
  cidr_block = var.cidr
  enable_dns_hostnames = true
  tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id
  depends_on = [aws_vpc.main]
}

resource "aws_subnet" "public" {
  cidr_block = cidrsubnet(cidrsubnet(aws_vpc.main.cidr_block, 2, 0), var.newbits, count.index)
  vpc_id = aws_vpc.main.id
  availability_zone = element(data.aws_availability_zones.available.names, count.index)
  count = length(data.aws_availability_zones.available.names)
  depends_on = [aws_vpc.main]

  tags = {
    Name = "public-${element(data.aws_availability_zones.available.names, count.index)}"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared",
    "kubernetes.io/role/elb"                    = "1"
  }
}

resource "aws_default_route_table" "public" {
  default_route_table_id = aws_vpc.main.default_route_table_id
  depends_on = [aws_vpc.main]
  tags = {
    Name = "public"
  }
}

resource "aws_route" "public" {
  route_table_id = aws_default_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.igw.id
  depends_on = [aws_default_route_table.public, aws_internet_gateway.igw]
}

resource "aws_route_table_association" "public" {
  route_table_id = aws_default_route_table.public.id
  subnet_id = element(aws_subnet.public[*].id, count.index)
  count = length(aws_subnet.public[*].id)
  depends_on = [aws_default_route_table.public, aws_subnet.public]
}

resource "aws_eip" "nat" {
  vpc = true
  count = length(aws_subnet.public)
}

resource "aws_nat_gateway" "nat_gw" {
  allocation_id = element(aws_eip.nat[*].id, count.index)
  subnet_id = element(aws_subnet.public[*].id, count.index)
  count = length(aws_subnet.public)
  depends_on = [aws_eip.nat, aws_subnet.public]
  tags = {
    Name = "nat-${element(aws_subnet.public[*].availability_zone, count.index)}"
  }
}

resource "aws_subnet" "private" {
  cidr_block = cidrsubnet(cidrsubnet(aws_vpc.main.cidr_block, 2, 1), var.newbits, count.index)
  vpc_id = aws_vpc.main.id
  availability_zone = element(data.aws_availability_zones.available.names, count.index)
  count = length(data.aws_availability_zones.available.names)
  depends_on = [aws_vpc.main]
  tags = {
    Name = "private-${element(data.aws_availability_zones.available.names, count.index)}"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared",
    "kubernetes.io/role/internal-elb"           = "1"
  }
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id
  count = length(aws_subnet.private)

  tags = {
    Name = element(aws_subnet.private[*].tags.Name, count.index)
  }
}

resource "aws_route" "private-default-nat" {
  route_table_id = element(aws_route_table.private[*].id, count.index)
  nat_gateway_id = element(aws_nat_gateway.nat_gw[*].id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  count = length(aws_route_table.private)
  depends_on = [aws_nat_gateway.nat_gw, aws_route_table.private]
}

resource "aws_route_table_association" "private" {
  route_table_id = element(aws_route_table.private[*].id, count.index)
  subnet_id = element(aws_subnet.private[*].id, count.index)
  count = length(aws_route_table.private)
  depends_on = [aws_route_table.private, aws_subnet.private]
}