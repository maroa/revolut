resource "aws_security_group" "eks-cluster" {
  name = "eks-cluster"
  vpc_id = aws_vpc.main.id
  depends_on = [aws_vpc.main]

  tags = {
    Name = "eks-cluster"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    protocol = "tcp"
    to_port = 443
    cidr_blocks = [local.workstation_external_cidr]
    description = "API access"
  }
}

resource "aws_iam_role" "eks-cluster" {
  name = "eks-cluster"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "cluster-EKSpolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks-cluster.name
  depends_on = [aws_iam_role.eks-cluster]
}

resource "aws_iam_role_policy_attachment" "cluster-EKSsvcpolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.eks-cluster.name
  depends_on = [aws_iam_role.eks-cluster]
}

resource "aws_eks_cluster" "eks" {
  name     = var.cluster_name
  role_arn = aws_iam_role.eks-cluster.arn

  vpc_config {
    security_group_ids = [aws_security_group.eks-cluster.id]
    subnet_ids         = aws_subnet.private[*].id
  }

  depends_on = [
    aws_iam_role_policy_attachment.cluster-EKSpolicy,
    aws_iam_role_policy_attachment.cluster-EKSsvcpolicy,
    aws_security_group.eks-cluster,
    aws_subnet.private
  ]
}