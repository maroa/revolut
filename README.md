# Revolut Home task
![Diagram](app-diagram.png)


Repository contains:

* hello-app application writen in Python using Postgresql as storage
* terraform for infrastructure deployment
* k8s files for application deployment

Pre-requirements:

* AWS account
* AWS service account with admin access
* Terraform installed, version >0.12.
* AWS CLI installed and credentials configured
* Kubectl installed 
* Docker installed and running
* jq installed
* Internet Connection

How to:

 1\. Clone repository

```git clone https://maroa@bitbucket.org/maroa/revolut.git```

 2\. Execute terraform

```
cd terraform
terraform init
terraform plan
terraform apply -parallelism=50 -auto-approve
```

 3\. Once the deploy is over and the service is running you will be able to retrieve the loadbalancer url for tests

```kubectl get ingress/hello-ingress -n hello-app -o json | jq -r .status.loadBalancer.ingress[0].hostname```

 4\. Using above information
    
```curl -X PUT -H "Content-type: application/json" http://<alb-url>/hello/<name> -d '{"dateOfBirth":"<YYYY-MM-DD>"}'``` TO INSERT DATA
    
```curl -X GET -H "Content-type: application/json" http://<alb-url>/hello/<name>``` TO GET DATA
    
```curl -X GET -H "Content-type: application/json" http://<alb-url>/health/status``` HEALTHCHECK VALIDATING DB CONNECTION

 5\. Finally destroy the infrastructure after tests
 
```terraform destroy```

## Testing Locally

 1\. Go to app directory 

```cd ..\app```

 2\. Run 

```docker-compose up```

 3\. You are able to test locally executing.

```curl -X PUT -H "Content-type: application/json" http://localhost:5000/hello/<name> -d '{"dateOfBirth":"<YYYY-MM-DD>"}'``` TO INSERT DATA

```curl -X GET -H "Content-type: application/json" http://localhost:5000/hello/<name>``` TO GET DATA

```curl -X GET -H "Content-type: application/json" http://localhost:5000/health/status``` HEALTHCHECK VALIDATING DB CONNECTION