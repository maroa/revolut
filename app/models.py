from flask_sqlalchemy import SQLAlchemy
from datetime import date

db = SQLAlchemy()


class User(db.Model):
    __tablename__ = 'users'

    def __init__(self, username, birthdate):
        self.username = username
        self.birthdate = birthdate

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(), unique=True, nullable=False)
    birthdate = db.Column(db.Date(), nullable=False)

    def hasBirthdayToday(self):
        today = date.today()
        if today.day == self.birthdate.day and today.month == self.birthdate.month:
            return True
        return False

    def daysToBirthDay(self):
        today = date.today()
        birthday = self.birthdate.replace(year=today.year)
        delta = birthday - today
        if delta.days < 0:
            nextYear = today.year + 1
            nextYearBirth=self.birthdate.replace(year=nextYear)
            delta = nextYearBirth - today
            return delta.days
        else:
            return delta.days

    @staticmethod
    def findByUsername(username):
        return User.query.filter_by(username=username).first()
