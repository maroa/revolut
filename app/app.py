from Config import Config
from controlers import Hello, Health
from flask import Flask
from flask_restful import Api
from models import db

app = Flask(__name__)
app.config.from_object(Config)

db.init_app(app)

with app.app_context():
    db.create_all()

api = Api(app)
api.add_resource(Hello, '/hello/<string:username>')
api.add_resource(Health, '/health/status')

app.run(host='0.0.0.0')
