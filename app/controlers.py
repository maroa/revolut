from flask import current_app
from flask_restful import Resource, abort, reqparse, inputs
from models import db, User
from datetime import date


def isUsernameAlpha(f):
    def decorator(*args, **kwargs):
        if not kwargs['username'].isalpha():
            abort(400, message={"username": "must contains only letters"})
        return f(*args, **kwargs)
    return decorator


class Hello(Resource):
    method_decorators = [isUsernameAlpha]

    def get(self, username):
        user = User.findByUsername(username)
        if not user:
            abort(404, message={"username": "doesn't exists"})
        if user.hasBirthdayToday():
            return {'message': 'Hello, {}! Happy birthday!'.format(username)}
        else:
            return {'message': "Hello, {}! Your birthday is in {} day(s)".format(username, user.daysToBirthDay())}

    def put(self, username):
        parser = reqparse.RequestParser()
        parser.add_argument('dateOfBirth', type=inputs.date, help='only YYYY-MM-DD format is supported')
        dateOfBirth = parser.parse_args(strict=True)['dateOfBirth']
        today = date.today()
        if not dateOfBirth.date() < today:
            abort(400, mesaage={'dateOfBirth': 'needs to be before today'})
        user = User.findByUsername(username)
        if user:
            user.birthdate = dateOfBirth
            db.session.commit()
        else:
            user = User(username, dateOfBirth)
            db.session.add(user)
            db.session.commit()
        return '', 204


class Health(Resource):
    def get(self):
        try:
            db.session.execute('SELECT 1')
            return {"status": "OK"}, 200
        except Exception as e:
            current_app.logger.error(e)
            return {"status": "ERROR"}, 500
